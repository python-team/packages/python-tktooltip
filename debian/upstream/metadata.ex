# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/python-tktooltip/issues
# Bug-Submit: https://github.com/<user>/python-tktooltip/issues/new
# Changelog: https://github.com/<user>/python-tktooltip/blob/master/CHANGES
# Documentation: https://github.com/<user>/python-tktooltip/wiki
# Repository-Browse: https://github.com/<user>/python-tktooltip
# Repository: https://github.com/<user>/python-tktooltip.git
